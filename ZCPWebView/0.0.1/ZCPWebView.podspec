Pod::Spec.new do |spec|

  spec.name          = "ZCPWebView"
  spec.version       = "0.0.1"
  spec.author        = { "朱超鹏" => "z164757979@163.com" }
  spec.license       = { :type => 'MIT', :file => 'LICENSE' }
  spec.homepage      = "https://gitlab.com/Malygos/ZCPWebView"
  spec.source        = { :git => "https://gitlab.com/Malygos/ZCPWebView.git", :tag => "#{spec.version}" }
  spec.summary       = "ZCP ZCPWebView."
  spec.description   = <<-DESC
                       ZCP ZCPWebView. pa hao fang router
                       DESC

  spec.platform      = :ios, '9.0'
  spec.ios.deployment_target = '9.0'
  spec.module_name   = 'ZCPWebView'
  spec.framework     = 'Foundation', 'UIKit'
  
  if ENV['IS_SOURCE']
    spec.source_files  = 'Source/*.{h,m}'
    spec.subspec 'JSBridge' do |subspec_jsbridge|
      subspec_jsbridge.source_files = 'Source/JSBridge/**/*.{h,m}'
    end
  end

  if ENV['IS_FRAMEWORK']
    spec.vendored_frameworks = 'Framework/ZCPWebView.framework'
  end

  spec.dependency 'ZCPCategory'
  spec.dependency 'OpenUDID'

end
